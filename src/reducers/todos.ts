import { ADD_TODO } from '../types/actionTypes';

interface Todo {
    text: string;
}

type Action = {
    type: typeof ADD_TODO;
    text: string;
};

export default function todos(state: Todo[] = [], action: Action): Todo[] {
    switch (action.type) {
        case ADD_TODO:
            return [...state, { text: action.text }];
        default:
            return state;
    }
}
