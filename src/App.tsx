import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addTodo } from './actions/todos';

const App: React.FC = () => {
    const [text, setText] = useState('');
    const dispatch = useDispatch();
    const todos = useSelector((state: any) => state);

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        dispatch(addTodo(text));
        setText('');
    };

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input value={text} onChange={e => setText(e.target.value)} />
                <button type="submit">Add Todo</button>
            </form>
            <ul>
                {todos.map((todo: any, index: number) => (
                    <li key={index}>{todo.text}</li>
                ))}
            </ul>
        </div>
    );
}

export default App;
