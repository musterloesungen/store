import { ADD_TODO } from '../types/actionTypes';

export function addTodo(text: string) {
    return {
        type: ADD_TODO,
        text
    };
}
